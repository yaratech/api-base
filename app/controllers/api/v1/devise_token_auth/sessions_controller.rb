module Api::V1::DeviseTokenAuth
  class SessionsController < DeviseTokenAuth::SessionsController

    def create
      @email = resource_params[:email]
      @user = resource_class.find_by_email(@email.to_s.strip.downcase) 

      if @user and valid_params? and @user.valid_password?(resource_params[:password]) and @user.confirmed?
        # create client id
        @client_id = SecureRandom.urlsafe_base64(nil, false)
        @token     = SecureRandom.urlsafe_base64(nil, false)
        @token_crypt = BCrypt::Password.create(@token)

        @user.tokens[@client_id] = {
          token: BCrypt::Password.create(@token),
          expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
        }        

        @user.save

        self.headers['Access-Token'] = @token.to_s
        self.headers['Client'] = @client_id.to_s
        self.headers['uid'] = @user.uid.to_s

        @push_token = TokenDevice.where("push_token = ? and user_id = ?", resource_params[:push_token], @user.id)
        if @push_token.count == 0
         @tokendevice = TokenDevice.new() 
         @tokendevice.push_token = params[:push_token]
         @tokendevice.user_id = @user.id
         @tokendevice.save
        end  

        render json: {
          success: true,
          data: @user.as_json
        }

      elsif @user and not @user.confirmed?
        render json: {
          success: false,
          errors: [
            "Se envió un correo electrónico de confirmación a su cuenta en #{@user.email}. "+
            "Debe seguir las instrucciones del correo electrónico antes de que su cuenta "+
            "Puede ser activado"
          ]
        }, status: 401

      else
        render json: {
          success: false,
          errors: ["Identidad o contraseña no válida."]
        }, status: 401
      end
    end

    def resource_params
      params.permit(:email, :password, :push_token)
    end

    def valid_params?
      resource_params[:password] && resource_params[:email] 
    end

  end
end
