class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_action :configure_permitted_parameters, if: :devise_controller?

  private

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up)        << :name
      devise_parameter_sanitizer.for(:sign_up)        << :country_id
      devise_parameter_sanitizer.for(:sign_up)        << :type
    end

end
