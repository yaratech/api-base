class HelpUs < ApplicationMailer

  def notification(user, message)
    @user = user
    @message = message
    mail(to: ["luelher@gmail.com","luiseloyherhahdez@gmail.com"], subject: 'Mensaje de Colaborador')
  end

end
