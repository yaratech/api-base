class TokenDevice < ActiveRecord::Base
  belongs_to :user, class_name: "Users::User"

  validates :push_token, :user_id,   presence: true

  rails_admin do
    list do
      field :user
      field :push_token
      field :created_at
    end
    edit do
      field :user
      field :push_token
      field :created_at
    end
  end

end
