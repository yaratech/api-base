# config valid only for current version of Capistrano
lock "3.8.1"

set :application, "SmShare"
set :repo_url, "git@bitbucket.org:crackapp/crackappapi.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/crack/app"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rvm_type, :user
set :rvm_ruby_version, '2.2.0@crackapp'

set :keep_releases, 2

set :slackistrano, {
 channel: '#notifications',
 webhook: 'https://hooks.slack.com/services/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'
}

append :linked_files, 'config/database.yml', 'config/secrets.yml'

namespace :deploy do
  after :finishing, 'apache:reload'
  after :rollback, 'apache:reload'
end