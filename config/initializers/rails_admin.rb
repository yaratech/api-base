# RailsAdmin config file. Generated on November 25, 2013 08:24
# See github.com/sferik/rails_admin for more informations

RailsAdmin.config do |config|

  config.authenticate_with do
    warden.authenticate! scope: :admin
  end
  config.current_user_method(&:current_admin)
  ################  Global configuration  ################

  # Set the admin name here (optional second array element will appear in red). For example:
  config.main_app_name = ['SmShare', 'Admin']
  # or for a more dynamic name:
  # config.main_app_name = Proc.new { |controller| [Rails.application.engine_name.titleize, controller.params['action'].titleize] }

  # If you want to track changes on your models:
  # config.audit_with :history, 'Administrador'
  
  # Or with a PaperTrail: (you need to install it first)
  # config.audit_with :paper_trail, 'Users::Admin'
  
  # Autorización con CanCan
  # config.authorize_with :cancan #, AdminAbility
  
  # Display empty fields in show views:
  # config.compact_show_view = false

  # Number of default rows per-page:
  # config.default_items_per_page = 20

  # Exclude specific models (keep the others):
  config.excluded_models = []

  # Include specific models (exclude the others):
  # config.included_models = ['Administrador', 'Usuario']

  # Label methods for model instances:
  # config.label_methods << :description # Default is [:name, :title]
  config.label_methods << :name
  config.label_methods << :description


  config.actions do
    # root actions
    dashboard                     # mandatory
    # collection actions 
    index                         # mandatory
    new
    history_index
    bulk_delete
    # member actions
    edit
    delete
  end

end
