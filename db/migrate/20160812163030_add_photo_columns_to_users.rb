class AddPhotoColumnsToUsers < ActiveRecord::Migration
  def up
    remove_column :users, :photo
    add_attachment :users, :photo
  end

  def down
    remove_attachment :users, :photo
    add_column :users, :photo, :string
  end
end
