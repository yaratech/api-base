class AddProviderUser < ActiveRecord::Migration
  def change
    add_column :users, :oauth_token, :string
    add_column :users, :oauth_expires_at, :datetime
  end
  def self.down  
    remove_column :users, :oauth_token
    remove_column :users, :oauth_expires_at
  end
end
