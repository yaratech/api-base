class AddUserIdProviderFacebook < ActiveRecord::Migration
  def change
    add_column :users, :user_id_provider, :string 
  end
  def self.down  
    remove_column :users, :user_id_provider   
  end
end
