class CreateTokenDevice < ActiveRecord::Migration
  def change
    create_table :token_devices do |t|
    	 t.string :push_token
    	 t.timestamps null: false
    end
    add_reference :token_devices, :user, index: true
    add_foreign_key :token_devices, :users
  end

   def self.down  
    remove_foreign_key :token_devices, :users 
    drop_table :token_devices

  end
end
