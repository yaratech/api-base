class AddUniqueTokenDevice < ActiveRecord::Migration
  def change
  	add_index :token_devices, [:push_token, :user_id], :unique => true
  end
end
