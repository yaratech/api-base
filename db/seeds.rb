# migrate user
# puts "Users"
seed_file = Rails.root.join('db', 'seeds', 'users.yml')
config = YAML::load_file(seed_file)
Users::User.create!(config)

# migrate Match
first_user = Users::User.just_user.first
last_user = Users::User.just_user.last

@client_id = SecureRandom.urlsafe_base64(nil, false)
@token     = SecureRandom.urlsafe_base64(nil, false)
@token_crypt = BCrypt::Password.create(@token)

first_user.tokens[@client_id] = {
  token: BCrypt::Password.create(@token),
  expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
}        
first_user.token_devices << TokenDevice.new(push_token: "12345678")
first_user.save



admin = Users::Admin.new
admin.name = "Administrador"
admin.email = "admin@dominio.com"
admin.password = '12345678'
admin.type = "Users::Admin"
admin.status = 'A'

admin.save
