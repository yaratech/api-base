module SimpleTokenAuthentication
  class DeviseFallbackHandler

    # fix bug
    def authenticate_entity!(controller, entity)
      controller.send("authenticate_user!".to_sym)
    end
  end
end