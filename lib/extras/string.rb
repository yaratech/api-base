class String
  def is_float?
    true if Float(self) rescue false
  end

  def is_integer?
    true if Integer(self) rescue false
  end

end