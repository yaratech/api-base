namespace :alerts do

  desc "Tasks For Schedule"

  task :welcome => :environment do
    puts "Notifications welcome_crackapp"
    require 'fcm'
    fcm = FCM.new(FCM_KEY)
    @title = "Dominio"       

    @TokenDevice = TokenDevice.joins(:user).includes(:user).where("users.push_on=true")

    @TokenDevice.each_with_index do |device, i|   
      registration_ids = []  
      @body = "Gracias #{device.user.name} por usar Dominio, Visita http://dominio.com para saber más"
      options = {:notification=>{:body=>@body, :title=>@title}}                    
      registration_ids[i] = device.push_token
      fcm.send(registration_ids, options)
    end

  end

end
