require 'test_helper'
require "custom_test_case"

class HomeControllerTest < CustomTestCase

  test "Landing Page" do

    get :index

    assert_response :success

    assert_match "Hola Mundo", @response.body

  end



end
