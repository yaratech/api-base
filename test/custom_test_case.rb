class CustomTestCase < ActionController::TestCase

  include Devise::TestHelpers                          
  include Warden::Test::Helpers  

  Warden.test_mode!                                    

  def teardown                                         
    Warden.test_reset!                                 
  end  

  def autenticated_user

    user = Users::User.first

    @resource_auth_headers = user.create_new_auth_token
    @token     = @resource_auth_headers['access-token']
    @client_id = @resource_auth_headers['client']    

    Warden.on_next_request do |proxy|
      proxy.set_user(user)
    end    

    return user
  end

  def load_seeds
    Rails.application.load_seed
  end
end